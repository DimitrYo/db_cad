Лабораторная работа #1-3
Мета роботи: 
Навчитися створювати діаграми таблиць і зв’язки між ними. Вивчити основні конструкції SQL, 
                понятяя ссилочної цілісності.
Описання вимог предметної області до цілісності даних. 


	DROP TABLE IF EXISTS Players CASCADE;
	DROP TABLE IF EXISTS PlayersAdditionalInfo CASCADE;
	DROP TABLE IF EXISTS Games CASCADE;
	DROP TABLE IF EXISTS MessagesPrivate CASCADE;
	DROP TABLE IF EXISTS MessagesGames CASCADE;
	DROP TABLE IF EXISTS Rounds CASCADE;
	DROP TABLE IF EXISTS Tournaments CASCADE;
	DROP TABLE IF EXISTS Contracts CASCADE;
	DROP TABLE IF EXISTS RoundsContracts CASCADE;
	DROP TABLE IF EXISTS Reffil CASCADE;
	
	  CREATE TABLE Players
  (
    login text , --1
    password text NOT NULL,
    email text NOT NULL, 
    first_name text NOT NULL,
    last_name text NOT NULL,
    adress text NOT NULL,
    birthday date,
    regdate date, 
    PRIMARY KEY (login), --1
    CHECK (login ~* '^[A-Za-z0-9]+$'), --2
    CHECK (email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$'), --3
    CHECK (first_name ~* '^[A-Za-z-]+$'), --4
    CHECK (last_name ~* '^[A-Za-z-]+$'), --5
	--CHECK ( regdate >= birthday) --5
  ); 

  
    CREATE TABLE  PlayersAdditionalInfo 
  (
    login text, -- REFERENCES Players (login), --6 //make trig
    gameplay integer NOT NULL,
    gamewin integer  NOT NULL,
	points integer NOT NULL, --rate
    contracts integer NOT NULL, 
    CHECK (points >= 0), --7
    CHECK (gameplay >= 0), --8
    CHECK (gamewin >= 0), --9
    CHECK (contracts >= 0)  --, --10
	--CHECK (gamewin <= gameplay) --11
  ); 
  
      CREATE TABLE  Games 
  (
    id_game SERIAL PRIMARY KEY, --13
    game_players text[] NOT NULL, --14
    ids_rounds integer[] NOT NULL, --15
    time_for_round integer NOT NULL CHECK(time_for_round > 0), --16
    rate integer NOT NULL CHECK(rate > 0), --17
    winners text[] NOT NULL, --18
    DTstart timestamp
  ); 
  
     CREATE TABLE  MessagesPrivate 
  (
    id_mes SERIAL PRIMARY KEY , --19
    sender text  REFERENCES Players (login) ON DELETE CASCADE, --20
    receiver text  REFERENCES Players (login) ON DELETE CASCADE, --21
    msg text NOT NULL, --22
    DTsend timestamp, 
    CHECK (sender != receiver) --23
  ); 
  
    CREATE TABLE  MessagesGames 
  (
    id_gammes SERIAL PRIMARY KEY, --24
    sender text REFERENCES Players (login) ON DELETE CASCADE, --25
    receiver_game integer  REFERENCES  Games  (id_game) ON DELETE CASCADE, --26
    msg text NOT NULL, --27
    DTsend timestamp
  );
  
    CREATE TABLE  Rounds 
  (
    id_round SERIAL PRIMARY KEY , --28
	id_game integer REFERENCES  Games  (id_game) ON DELETE CASCADE,
    question text NOT NULL, --29
    players text[] NOT NULL, --30
    answers boolean[] NOT NULL , --31
    winners text[] NOT NULL,  --32
    DTstart timestamp
  );
  
   CREATE TABLE  Tournaments 
  (
    id_tournament SERIAL PRIMARY KEY , --33
    id_games integer[]  NOT NULL, --34
    players text[] NOT NULL, --35
    winners text[] NOT NULL, --36
    prize integer NOT NULL, --37
    DTstart timestamp 
  );
  
   CREATE TABLE  Contracts 
  (
    id_contract SERIAL PRIMARY KEY, --38
    id_game integer NOT NULL, --39
    players text[] NOT NULL, --40
    prize_divide integer[] NOT NULL, --40
    contacton integer NOT NULL, --41
    contactoff integer NOT NULL, --42
    id_contract_round integer NOT NULL --43
  );
  
     CREATE TABLE  RoundsContracts 
  (
    id_round_contract SERIAL PRIMARY KEY ,--44
    id_games integer NOT NULL, --45
    players text[] NOT NULL, --45
    answers boolean[] NOT NULL --46
  );
  
     CREATE TABLE  Reffil 
  (
    id_reffil serial PRIMARY KEY , --47
    id_player text REFERENCES Players (login), --48
    money integer NOT NULL, --49
    rate integer NOT NULL --50
  );
  
  
  
  
  CREATE OR REPLACE FUNCTION trig_date_funk() RETURNS trigger AS $trig_date$
 DECLARE
 BEGIN
	IF (SELECT login FROM Players WHERE login = NEW.login) IS NULL THEN
        RAISE EXCEPTION 'this foreign key login does not exist';
	END IF;
	IF (NEW.gamewin > NEW.gameplay) THEN
		RAISE EXCEPRION 'Impossible gameplay and gamewin parametrs';
	END IF;
  RETURN NEW;
 END;
$trig_date$ LANGUAGE  plpgsql;
CREATE TRIGGER trig_date BEFORE INSERT OR UPDATE ON PlayersAdditionalInfo FOR each ROW EXECUTE PROCEDURE trig_date_funk();



CREATE TABLE  games_reg(
        user_name               text NOT NULL,
        date_edit               timestamp NOT NULL,
        operation               text NOT NULL,                                                                                       
		id_game integer NOT NULL, --13
		game_players text[] NOT NULL, --14
		ids_rounds integer[] NOT NULL, --15
		time_for_round integer NOT NULL , --16
		rate integer NOT NULL , --17
		winners text[] NOT NULL, --18
		DTstart timestamp
);


CREATE OR REPLACE FUNCTION trig_games_reg_funk() RETURNS trigger AS $trig_games_reg$
 DECLARE
 BEGIN
   IF (TG_OP = 'DELETE') THEN
          INSERT INTO deposit_reg SELECT user, now(), TG_OP, OLD.id_game, OLD.game_players, 
          OLD.time_for_round, OLD.rate, OLD.winners,OLD.DTstart;
          RETURN OLD;
   ELSIF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
          INSERT INTO deposit_reg SELECT user, now(), TG_OP, NEW.id_game, NEW.game_players, 
          NEW.time_for_round, NEW.rate, NEW.winners,NEW.DTstart;
          RETURN NEW;
   END IF;
 END;
$trig_deposit_reg$ LANGUAGE  plpgsql;
CREATE TRIGGER trig_games_reg AFTER INSERT OR UPDATE OR DELETE ON deposit FOR each ROW EXECUTE PROCEDURE trig_games_reg_funk();

--data.sql
INSERT INTO Players  VALUES
	('play1','pasw','pl@g.ua','vasya','vasya','Kiev','1980-12-12','1990-11-11'),
	('play2','pasw2','pl2@g.ua','vasya','vasya','Kiev','1982-12-12','1993-11-11'),
	('play3','pasw3','pl3@g.ua','vasya','vasya','Kiev','1983-12-12','1993-11-11');
	
INSERT INTO PlayersAdditionalInfo  VALUES
	('play1',50,1,1,0);
INSERT INTO PlayersAdditionalInfo  VALUES
	('play2',60,1,0,0);
INSERT INTO PlayersAdditionalInfo  VALUES
	('play3',60,1,0,0);
	
INSERT INTO Games VALUES 
	(DEFAULT,  '{"play1","play2","play3"}' , '{}' , 500 , 100 , '{}' , '1999-01-08 04:05:06'); 
	

--data_fail.sql

INSERT INTO Players  VALUES
	('play1','pasw','pl@g.ua','vasya','vasya','Kiev','1980-12-12','1990-11-11');
	/*  in primary key */
INSERT INTO Players  VALUES
	('play1','pasw2','pl2@g.ua','vasya','vasya','Kiev','1982-12-12','1993-11-11'); 
	/* in surname */
INSERT INTO Players  VALUES
	('play3','pasw','pl@g.ua','vasy1a','vasya','Kiev','1980-12-12','1990-11-11');
	/* in name */
INSERT INTO Players  VALUES
	('play4','pasw2','pl2@g.ua','vasya','vasy1a','Kiev','1982-12-12','1993-11-11');

	
  
  