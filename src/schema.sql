﻿	DROP TABLE IF EXISTS Players CASCADE;
	DROP TABLE IF EXISTS PlayersAdditionalInfo CASCADE;
	DROP TABLE IF EXISTS Games CASCADE;
	DROP TABLE IF EXISTS MessagesPrivate CASCADE;
	DROP TABLE IF EXISTS MessagesGames CASCADE;
	DROP TABLE IF EXISTS Rounds CASCADE;
	DROP TABLE IF EXISTS Tournaments CASCADE;
	DROP TABLE IF EXISTS Contracts CASCADE;
	DROP TABLE IF EXISTS RoundsContracts CASCADE;
	DROP TABLE IF EXISTS Reffil CASCADE;
	
	  CREATE TABLE Players
  (
    login text , --1
    password text NOT NULL,
    email text NOT NULL, 
    first_name text NOT NULL,
    last_name text NOT NULL,
    adress text NOT NULL,
    birthday date,
    regdate date, 
    PRIMARY KEY (login), --1
    CHECK (login ~* '^[A-Za-z0-9]+$'), --2
    CHECK (email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$'), --3
    CHECK (first_name ~* '^[A-Za-z-]+$'), --4
    CHECK (last_name ~* '^[A-Za-z-]+$')--, --5
	--CHECK ( regdate >= birthday) --5
  ); 

  
    CREATE TABLE  PlayersAdditionalInfo 
  (
    login text, -- REFERENCES Players (login), --6 //make trig
    gameplay integer NOT NULL,
    gamewin integer  NOT NULL,
	points integer NOT NULL, --rate
    contracts integer NOT NULL, 
    CHECK (points >= 0), --7
    CHECK (gameplay >= 0), --8
    CHECK (gamewin >= 0), --9
    CHECK (contracts >= 0)  --, --10
	--CHECK (gamewin <= gameplay) --11
  ); 
  
      CREATE TABLE  Games 
  (
    id_game SERIAL PRIMARY KEY, --13
    game_players text[] NOT NULL, --14
    ids_rounds integer[] NOT NULL, --15
    time_for_round integer NOT NULL CHECK(time_for_round > 0), --16
    rate integer NOT NULL CHECK(rate > 0), --17
    winners text[] NOT NULL, --18
    DTstart timestamp
  ); 
  
     CREATE TABLE  MessagesPrivate 
  (
    id_mes SERIAL PRIMARY KEY , --19
    sender text  REFERENCES Players (login) ON DELETE CASCADE, --20
    receiver text  REFERENCES Players (login) ON DELETE CASCADE, --21
    msg text NOT NULL, --22
    DTsend timestamp, 
    CHECK (sender != receiver) --23
  ); 
  
    CREATE TABLE  MessagesGames 
  (
    id_gammes SERIAL PRIMARY KEY, --24
    sender text REFERENCES Players (login) ON DELETE CASCADE, --25
    receiver_game integer  REFERENCES  Games  (id_game) ON DELETE CASCADE, --26
    msg text NOT NULL, --27
    DTsend timestamp
  );
  
    CREATE TABLE  Rounds 
  (
    id_round SERIAL PRIMARY KEY , --28
	id_game integer REFERENCES  Games  (id_game) ON DELETE CASCADE,
    question text NOT NULL, --29
    players text[] NOT NULL, --30
    answers boolean[] NOT NULL , --31
    winners text[] NOT NULL,  --32
    DTstart timestamp
  );
  
   CREATE TABLE  Tournaments 
  (
    id_tournament SERIAL PRIMARY KEY , --33
    id_games integer[]  NOT NULL, --34
    players text[] NOT NULL, --35
    winners text[] NOT NULL, --36
    prize integer NOT NULL, --37
    DTstart timestamp 
  );
  
   CREATE TABLE  Contracts 
  (
    id_contract SERIAL PRIMARY KEY, --38
    id_game integer NOT NULL, --39
    players text[] NOT NULL, --40
    prize_divide integer[] NOT NULL, --40
    contacton integer NOT NULL, --41
    contactoff integer NOT NULL, --42
    id_contract_round integer NOT NULL --43
  );
  
     CREATE TABLE  RoundsContracts 
  (
    id_round_contract SERIAL PRIMARY KEY ,--44
    id_games integer NOT NULL, --45
    players text[] NOT NULL, --45
    answers boolean[] NOT NULL --46
  );
  
     CREATE TABLE  Reffil 
  (
    id_reffil serial PRIMARY KEY , --47
    id_player text REFERENCES Players (login), --48
    money integer NOT NULL, --49
    rate integer NOT NULL --50
  );
  
  
CREATE ROLE dbsuperadmin WITH SUPERUSER CREATEROLE CREATEDB LOGIN ENCRYPTED PASSWORD '1';
CREATE ROLE dbadmin WITH CREATEROLE LOGIN ENCRYPTED PASSWORD '1';
CREATE ROLE moderator WITH LOGIN ENCRYPTED PASSWORD '1';
GRANT CONNECT ON DATABASE lab1 TO moderator;
GRANT CONNECT ON DATABASE lab1 TO dbadmin;
GRANT SELECT, INSERT, UPDATE ON ALL TABLES IN SCHEMA public TO dbadmin;
GRANT SELECT(    id_game , game_players , ids_rounds, time_for_round , rate) ON games TO moderator; 
GRANT SELECT(  id_round,  id_game , question , players, answers) ON rounds TO moderator;

  
  
  
  
  CREATE OR REPLACE FUNCTION trig_date_funk() RETURNS trigger AS $trig_date$
 DECLARE
 BEGIN
	IF (SELECT login FROM Players WHERE login = NEW.login) IS NULL THEN
        RAISE EXCEPTION 'this foreign key login does not exist';
	END IF;
	IF (NEW.gamewin > NEW.gameplay) THEN
		RAISE EXCEPTION 'Impossible gameplay and gamewin parametrs';
	END IF;
  RETURN NEW;
 END;
$trig_date$ LANGUAGE  plpgsql;
CREATE TRIGGER trig_date BEFORE INSERT OR UPDATE ON PlayersAdditionalInfo FOR each ROW EXECUTE PROCEDURE trig_date_funk();


DROP TABLE IF EXISTS games_reg;
CREATE TABLE  games_reg(
        user_name               text NOT NULL,
        date_edit               timestamp NOT NULL,
        operation               text NOT NULL,                                                                                       
		id_game integer NOT NULL, --13
		game_players text[] NOT NULL, --14
		ids_rounds integer[] NOT NULL, --15
		time_for_round integer NOT NULL , --16
		rate integer NOT NULL , --17
		winners text[] NOT NULL, --18
		DTstart timestamp
);


CREATE OR REPLACE FUNCTION trig_games_reg_funk() RETURNS trigger AS $trig_games_reg$
 DECLARE
 BEGIN
   IF (TG_OP = 'DELETE') THEN
          INSERT INTO deposit_reg SELECT user, now(), TG_OP, OLD.id_game, OLD.game_players, 
          OLD.time_for_round, OLD.rate, OLD.winners,OLD.DTstart;
          RETURN OLD;
   ELSIF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
          INSERT INTO deposit_reg SELECT user, now(), TG_OP, NEW.id_game, NEW.game_players, 
          NEW.time_for_round, NEW.rate, NEW.winners,NEW.DTstart;
          RETURN NEW;
   END IF;
 END;
$trig_games_reg$ LANGUAGE  plpgsql;
CREATE TRIGGER trig_games_reg AFTER INSERT OR UPDATE OR DELETE ON games_reg FOR each ROW EXECUTE PROCEDURE trig_games_reg_funk();

DROP VIEW IF EXISTs players_ok;
CREATE OR REPLACE VIEW players_ok
AS SELECT players.login, players.last_name, players.regdate 
FROM players
JOIN  PlayersAdditionalInfo ON (players.login = PlayersAdditionalInfo.login)
WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday);

GRANT SELECT ON players_ok TO dbsuperadmin, dbadmin, moderator;

CREATE OR REPLACE RULE players_ok_insert_settlement AS ON INSERT TO players_ok
        DO INSTEAD (
        INSERT INTO players (login,    last_name,regdate)
        VALUES (NEW.login, NEW.last_name,NEW.regdate);    
        );
		
CREATE OR REPLACE RULE players_ok_update_settlement AS ON UPDATE TO players_ok 
        DO INSTEAD (
        UPDATE players
        SET  last_name=NEW.last_name , regdate=NEW.regdate
                WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday)
       );
         
CREATE OR REPLACE RULE players_ok_delete_settlement AS ON DELETE TO players_ok
        DO INSTEAD (
        DELETE FROM players 
                WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday)
        );
		
CREATE OR REPLACE FUNCTION players_ok_func()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $function$
   BEGIN
        IF res = 'INSERT' THEN
                INSERT INTO players (login, players.password, email, first_name, last_name,adress,birthday,regdate)
        VALUES (NEW.login, NEW.password, NEW.email, NEW.first_name,NEW.last_name,NEW.adress,NEW.birthday,NEW.regdate);
                RETURN NEW;
        
        ELSIF res = 'UPDATE' THEN
                UPDATE players
		SET  email=NEW.email ,first_name=NEW.first_name , last_name=NEW.last_name , adress=NEW.adress ,birthday=NEW.birthday , regdate=NEW.regdate
                WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday);
                RETURN NEW;
        
        ELSIF res = 'DELETE' THEN
                DELETE FROM players 
                WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday);
        RETURN NULL;
      END IF;
      RETURN NEW;
    END;
$function$;

CREATE TRIGGER free_rooms
    INSTEAD OF INSERT OR UPDATE OR DELETE ON players_ok
    FOR EACH ROW EXECUTE PROCEDURE players_ok_func();







  
  
  