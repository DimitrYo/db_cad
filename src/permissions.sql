CREATE ROLE dbsuperadmin WITH SUPERUSER CREATEROLE CREATEDB LOGIN ENCRYPTED PASSWORD '1';
CREATE ROLE dbadmin WITH CREATEROLE LOGIN ENCRYPTED PASSWORD '1';
CREATE ROLE moderator WITH LOGIN ENCRYPTED PASSWORD '1';
GRANT CONNECT ON DATABASE lab1 TO moderator;
GRANT CONNECT ON DATABASE lab1 TO dbadmin;
GRANT SELECT, INSERT, UPDATE ON ALL TABLES IN SCHEMA public TO dbadmin;
GRANT SELECT(    id_game , game_players , ids_rounds, time_for_round , rate) ON games TO moderator; 
GRANT SELECT(  id_round,  id_game , question , players, answers) ON rounds TO moderator;

GRANT SELECT ON players_ok TO dbsuperadmin, dbadmin, moderator;