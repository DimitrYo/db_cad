DROP VIEW IF EXISTs players_ok;
CREATE OR REPLACE VIEW players_ok
AS SELECT players.login, players.last_name, players.regdate 
FROM players
JOIN  PlayersAdditionalInfo ON (players.login = PlayersAdditionalInfo.login)
WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday);

GRANT SELECT ON players_ok TO dbsuperadmin, dbadmin, moderator;

CREATE OR REPLACE RULE players_ok_insert_settlement AS ON INSERT TO players_ok
        DO INSTEAD (
        INSERT INTO players (login,    last_name,regdate)
        VALUES (NEW.login, NEW.last_name,NEW.regdate);    
        );
		
CREATE OR REPLACE RULE players_ok_update_settlement AS ON UPDATE TO players_ok 
        DO INSTEAD (
        UPDATE players
        SET  last_name=NEW.last_name , regdate=NEW.regdate
                WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday)
       );
         
CREATE OR REPLACE RULE players_ok_delete_settlement AS ON DELETE TO players_ok
        DO INSTEAD (
        DELETE FROM players 
                WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday)
        );
		
CREATE OR REPLACE FUNCTION players_ok_func()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $function$
   BEGIN
        IF res = 'INSERT' THEN
                INSERT INTO players (login, players.password, email, first_name, last_name,adress,birthday,regdate)
        VALUES (NEW.login, NEW.password, NEW.email, NEW.first_name,NEW.last_name,NEW.adress,NEW.birthday,NEW.regdate);
                RETURN NEW;
        
        ELSIF res = 'UPDATE' THEN
                UPDATE players
		SET  email=NEW.email ,first_name=NEW.first_name , last_name=NEW.last_name , adress=NEW.adress ,birthday=NEW.birthday , regdate=NEW.regdate
                WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday);
                RETURN NEW;
        
        ELSIF res = 'DELETE' THEN
                DELETE FROM players 
                WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday);
        RETURN NULL;
      END IF;
      RETURN NEW;
    END;
$function$;

CREATE TRIGGER free_rooms
    INSTEAD OF INSERT OR UPDATE OR DELETE ON players_ok
    FOR EACH ROW EXECUTE PROCEDURE players_ok_func();