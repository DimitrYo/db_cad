EXPLAIN ANALYZE  SELECT players.login, players.last_name, players.regdate 
FROM players
JOIN  PlayersAdditionalInfo ON (players.login = PlayersAdditionalInfo.login)
WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday);

EXPLAIN ANALYZE  SELECT players.login, players.last_name, players.regdate 
FROM players
JOIN  PlayersAdditionalInfo ON (players.login = PlayersAdditionalInfo.login)
WHERE  ( players.regdate  > players.birthday) OR ((players.last_name > 'c') AND (players.last_name < 'y'));

DROP INDEX IF EXISTS players_regdate_index;
DROP INDEX IF EXISTS players_birthday_index;
DROP INDEX IF EXISTS players_last_name_index;

CREATE INDEX players_regdate_index ON players(regdate);
CREATE INDEX players_birthday_index ON players(birthday);
CREATE INDEX players_last_name_index ON players(last_name);

EXPLAIN ANALYZE  SELECT players.login, players.last_name, players.regdate 
FROM players
JOIN  PlayersAdditionalInfo ON (players.login = PlayersAdditionalInfo.login)
WHERE (players.last_name > 'b') AND ( players.regdate  > players.birthday);

EXPLAIN ANALYZE  SELECT players.login, players.last_name, players.regdate 
FROM players
JOIN  PlayersAdditionalInfo ON (players.login = PlayersAdditionalInfo.login)
WHERE  ( players.regdate  > players.birthday) OR ((players.last_name > 'c') AND (players.last_name < 'y'));