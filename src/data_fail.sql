INSERT INTO Players  VALUES
	('play1','pasw','pl@g.ua','vasya','vasya','Kiev','1980-12-12','1990-11-11');
	/*  in primary key */
INSERT INTO Players  VALUES
	('play1','pasw2','pl2@g.ua','vasya','vasya','Kiev','1982-12-12','1993-11-11'); 
	/* in surname */
INSERT INTO Players  VALUES
	('play3','pasw','pl@g.ua','vasy1a','vasya','Kiev','1980-12-12','1990-11-11');
	/* in name */
INSERT INTO Players  VALUES
	('play4','pasw2','pl2@g.ua','vasya','vasy1a','Kiev','1982-12-12','1993-11-11');
